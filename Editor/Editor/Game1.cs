using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Windows.Forms;

namespace Editor {
	public class Game1 : Game {

		public class Tile {
			public Tile() {
				type = -1;
				subType = 0;
				subTypes = 0;
			}
			public int type;
			public int subType;
			public int subTypes;
		}

		const int hotbarWidth = 10;
		List<List<Tile>> grid = new List<List<Tile>>();
		List<Texture2D> tiles = new List<Texture2D>();
		List<int> hotbar = new List<int>(hotbarWidth);


		Texture2D lineTexture;

		int selectedTile = 0;
		const int offsetStep = 8;
		int xOffset = 0;
		int yOffset = 0;
		int tileSize = 32;
		const int tileSizeTex = 32;
		const int tileSizeHotbar = 32;
		const int roomUnitSize = 11;
		int gridHeight = 0;
		bool gridLines = true;

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		InputHelper inputHelper;

		public void DrawLine(SpriteBatch spriteBatch, Vector2 begin, Vector2 end, Color color, int width = 1) {
			Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length() + width, width);
			Vector2 v = Vector2.Normalize(begin - end);
			float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
			if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
			spriteBatch.Draw(lineTexture, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
		}

		public void DrawRectangle(SpriteBatch spriteBatch, Vector2 begin, Vector2 end, Color color) {
			spriteBatch.Draw(lineTexture, new Rectangle((int)begin.X, (int)begin.Y, (int)end.X - (int)begin.X, (int)end.Y - (int)begin.Y), color);
		}

		public Game1() {
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 1280;
			graphics.PreferredBackBufferHeight = 720;
			Content.RootDirectory = "Content";
			IsMouseVisible = true;
			Window.AllowUserResizing = true;


			inputHelper = new InputHelper();
		}

		protected override void Initialize() {
			base.Initialize();
		}

		protected override void LoadContent() {
			spriteBatch = new SpriteBatch(GraphicsDevice);

			string[] files = Directory.GetFiles("Tiles");

			foreach (string file in files) {
				if (file.EndsWith(".png")) {
					tiles.Add(Texture2D.FromStream(GraphicsDevice, File.OpenRead(file)));
				}
			}

			for (int i = 0; i < hotbarWidth; i ++) {
				if (i < tiles.Count) {
					hotbar.Add(i);
				} else {
					hotbar.Add(-1);
				}
				
			}
			lineTexture = new Texture2D(GraphicsDevice, 1, 1);
			lineTexture.SetData<Color>(new Color[] { Color.White });
		}

		protected override void UnloadContent() {
		}
		protected override void Update(GameTime gameTime) {
			inputHelper.Update(gameTime);
			base.Update(gameTime);
			if (!base.IsActive) {
				return;
			}
			//Place Tile and resize grid
			if (inputHelper.MouseLeftButtonPress()) {
				if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Space)) {
					int x = ((int)(inputHelper.MousePosition.X - xOffset) / tileSizeTex);
					int y = ((int)(inputHelper.MousePosition.Y - yOffset) / tileSizeTex);
					int selected = x + y * 20;
					hotbar[selectedTile] = (selected > -1 && selected < tiles.Count) ? selected : -1;
				} else {
					int xTile = (int)(inputHelper.MousePosition.X - xOffset) / tileSize;
					int yTile = (int)(inputHelper.MousePosition.Y - yOffset) / tileSize;
					if (xTile >= 0 && yTile >= 0) {
						if (xTile >= grid.Count) {
							grid.Resize((int)MathHelper.Max(0, xTile + 1));
							foreach (List<Tile> list in grid)
								list.Resize(gridHeight);
						}
						if (yTile + 1 >= gridHeight) {
							foreach (List<Tile> list in grid)
								list.Resize((int)MathHelper.Max(0, MathHelper.Max(yTile + 1, gridHeight)));
							gridHeight = yTile + 1;
						}
						grid[xTile][yTile].type = hotbar[selectedTile];
						if (grid[xTile][yTile].type != -1) {
							grid[xTile][yTile].subTypes = tiles[grid[xTile][yTile].type].Width / tileSize - 1;
						}
					}
				}
			}

			//Remove Tile and resize grid
			if (inputHelper.MouseRightButtonPress()) {
				int xTile = (int)(inputHelper.MousePosition.X - xOffset) / tileSize;
				int yTile = (int)(inputHelper.MousePosition.Y - yOffset) / tileSize;

				if (xTile >= 0 && yTile >= 0 && xTile < grid.Count && yTile < gridHeight) {
					grid[xTile][yTile].type = -1;

					int i = 0;
                    for (i = grid.Count - 1; i > 0; --i) {
						foreach(Tile tile in grid[i]) {
							if (tile.type != -1) {
								i += 1;
								goto Exit;
							}
						}
					}
					Exit:
					if (i < grid.Count) {
						grid.RemoveRange(i, grid.Count - i);
					}
					int height = 0;
					for (i = grid.Count - 1; i > 0; --i) {
						for (int j = grid[0].Count - 1; j > 0; --j) {
							if (grid[i][j].type != -1) {
								if (j > height) {
									height = j;
								}
							}
						}
					}
					height += 1;
					if (height < gridHeight) {
						foreach (List<Tile> column in grid) {
							column.RemoveRange(height, gridHeight - height);
						}
						gridHeight -= gridHeight - height;
					}
				}
			}

			if (inputHelper.ScrollWheel() != 0) {
				if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftControl)) {
					selectedTile = (int)MathHelper.Clamp(selectedTile + inputHelper.ScrollWheel(), 0, hotbar.Count - 1);
				} else if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift)) {
					tileSize = (int)MathHelper.Clamp(inputHelper.ScrollWheel() == 1 ? tileSize * 2 : (int)(tileSize * 0.5), 1, 16384);
					xOffset = inputHelper.ScrollWheel() == 1 ? xOffset * 2 : (int)(xOffset * 0.5);
					yOffset = inputHelper.ScrollWheel() == 1 ? yOffset * 2 : (int)(yOffset * 0.5);
				} else {
					int xTile = (int)(inputHelper.MousePosition.X - xOffset) / tileSize;
					int yTile = (int)(inputHelper.MousePosition.Y - yOffset) / tileSize;

					if (xTile >= 0 && yTile >= 0 && xTile < grid.Count && yTile < gridHeight) {
						grid[xTile][yTile].subType = (int)MathHelper.Clamp(grid[xTile][yTile].subType + inputHelper.ScrollWheel(), 0, grid[xTile][yTile].subTypes);
					}
				}
			}

			if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.W)) {
				yOffset += offsetStep;
			} else if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.S)) {
				yOffset -= offsetStep;
			}

			if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.A)) {
				xOffset += offsetStep;
			} else if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.D)) {
				xOffset -= offsetStep;
			}

			if (inputHelper.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Z)) {
				SaveLevel();
			}

			if (inputHelper.KeyPressed(Microsoft.Xna.Framework.Input.Keys.X)) {
				gridLines = !gridLines;
			}

			if (inputHelper.KeyPressed(Microsoft.Xna.Framework.Input.Keys.C)) {
				LoadLevel();
			}
		}
		protected override void Draw(GameTime gameTime) {
			GraphicsDevice.Clear(Color.Black);
			spriteBatch.Begin();

			//Draw tiles
			for (int i = 0; i < grid.Count; i++) {
				for (int j = 0; j < grid[i].Count; j++) {
					if (grid[i][j].type != -1) {
//						spriteBatch.Draw(tiles[grid[i][j].type], new Vector2(i * tileSize + xOffset, j * tileSize + yOffset), new Rectangle(tileSize * grid[i][j].subType, 0, tileSize, tileSize), Color.White);
						spriteBatch.Draw(tiles[grid[i][j].type], new Vector2(i * tileSize + xOffset, j * tileSize + yOffset), new Rectangle(tileSizeTex * grid[i][j].subType, 0, tileSizeTex, tileSizeTex),
							Color.White, 0f, Vector2.Zero, new Vector2((float)tileSize / tileSizeTex, (float)tileSize / tileSizeTex), SpriteEffects.None, 0f);
					}
				}
			}


			//Draw grid
			if (grid.Count > 0 && gridLines) {
				for (int i = 0; i < grid.Count; i++) {
					if (i % roomUnitSize == 0) {
						DrawLine(spriteBatch, new Vector2(i * tileSize + xOffset, yOffset), new Vector2(i * tileSize + xOffset, grid[i].Count * tileSize + yOffset), Color.Red, 1);
					} else {
						DrawLine(spriteBatch, new Vector2(i * tileSize + xOffset, yOffset), new Vector2(i * tileSize + xOffset, grid[i].Count * tileSize + yOffset), Color.White, 1);
					}
				}
				for (int j = 0; j < grid[0].Count; j++) {
					if (j % roomUnitSize == 0) {
						DrawLine(spriteBatch, new Vector2(xOffset, j * tileSize + yOffset), new Vector2(grid.Count * tileSize + xOffset, j * tileSize + yOffset), Color.Red, 1);
					} else {
						DrawLine(spriteBatch, new Vector2(xOffset, j * tileSize + yOffset), new Vector2(grid.Count * tileSize + xOffset, j * tileSize + yOffset), Color.White, 1);
					}
				}
				DrawLine(spriteBatch, new Vector2(xOffset, yOffset), new Vector2(grid.Count * tileSize + xOffset, yOffset), Color.White, 1);
				DrawLine(spriteBatch, new Vector2(grid.Count * tileSize + xOffset, yOffset), new Vector2(grid.Count * tileSize + xOffset, grid[0].Count * tileSize + yOffset), Color.White, 1);
				DrawLine(spriteBatch, new Vector2(xOffset, grid[0].Count * tileSize +  yOffset), new Vector2(grid.Count * tileSize + xOffset, grid[0].Count * tileSize + yOffset), Color.White, 1);
			}

			//Draw tile overview screen
			if (inputHelper.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Space)) {
				DrawRectangle(spriteBatch, new Vector2(0, 0), new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.Black);
				for (int i = 0; i < tiles.Count; i++) {
					spriteBatch.Draw(tiles[i], new Vector2(i * tileSizeTex + xOffset, yOffset), new Rectangle(0, 0, tileSizeTex, tileSizeTex), Color.White);
				}
			}

			//Draw hotbar
			int hotbarX1 = graphics.PreferredBackBufferWidth / 2 - (hotbarWidth / 2) * tileSizeHotbar;
			int hotbarX2 = graphics.PreferredBackBufferWidth / 2 + (hotbarWidth / 2) * tileSizeHotbar;
			int hotbarY = graphics.PreferredBackBufferHeight - tileSizeHotbar - 8;

			DrawRectangle(spriteBatch, new Vector2(hotbarX1, hotbarY), new Vector2(hotbarX2, hotbarY + tileSizeHotbar + 8), Color.DarkBlue);
			DrawRectangle(spriteBatch, new Vector2(hotbarX1 + selectedTile * tileSizeHotbar, hotbarY), new Vector2(hotbarX1 + (selectedTile + 1) * tileSizeHotbar, hotbarY + tileSizeHotbar), Color.Red);

			for (int i = 0; i < hotbar.Count; i++) {
				if (hotbar[i] != -1) {
					spriteBatch.Draw(tiles[hotbar[i]], new Vector2(hotbarX1, hotbarY + 4), new Rectangle(0, 0, tileSizeTex, tileSizeTex),
						Color.White, 0f, Vector2.Zero, new Vector2((float)tileSizeTex / tileSizeHotbar, (float)tileSizeTex / tileSizeHotbar), SpriteEffects.None, 0f);
				}
				hotbarX1 += tileSizeHotbar;
			}

			spriteBatch.End();
			base.Draw(gameTime);
		}

		void SaveLevel() {
			SaveFileDialog Dialog = new SaveFileDialog();
			Dialog.Title = "Save Text File";
			Dialog.Filter = "TXT files|*.txt";
			Dialog.InitialDirectory = @"C:\";
			if (Dialog.ShowDialog() == DialogResult.OK) {

				System.IO.File.WriteAllText(Dialog.FileName, string.Empty);
				using (System.IO.StreamWriter file = new System.IO.StreamWriter(Dialog.FileName, true)) {
					file.WriteLine(grid.Count + "," + gridHeight);

					for (int j = 0; j < gridHeight; j++) {
						for (int i = 0; i < grid.Count; i++) {
							if (grid[i][j].type == 0) {
								file.Write((i / roomUnitSize).ToString() + "," + (j / roomUnitSize).ToString() + ",");
								file.Write(i % roomUnitSize == 0 ? "w" : i % roomUnitSize == roomUnitSize - 1 ? "e" : j % roomUnitSize == 0 ? "n" : j % roomUnitSize == roomUnitSize - 1 ? "s" : "");
							}
						}
					}

					file.WriteLine("");

					for (int j = 0; j < gridHeight; j++) {
						for (int i = 0; i < grid.Count; i++) {
							if (grid[i][j].type == 1) {
								file.Write((i / roomUnitSize).ToString() + "," + (j / roomUnitSize).ToString() + ",");
								file.Write(i % roomUnitSize == 0 ? "w" : i % roomUnitSize == roomUnitSize - 1 ? "e" : j % roomUnitSize == 0 ? "n" : j % roomUnitSize == roomUnitSize - 1 ? "s" : "");
							}
						}
					}
					file.WriteLine("");

					for (int j = 0; j < gridHeight; j++) {
						for (int i = 0; i < grid.Count; i++) {

							file.Write(grid[i][j].type + " " + grid[i][j].subType + ",");
						}
						file.WriteLine();
					}
				}
			}
		}

		void LoadLevel() {
			OpenFileDialog Dialog = new OpenFileDialog();
			Dialog.Title = "Open Text File";
			Dialog.Filter = "TXT files|*.txt";
			Dialog.InitialDirectory = @"C:\";
			if (Dialog.ShowDialog() == DialogResult.OK) {
				string[] lines = File.ReadAllLines(Dialog.FileName);
				string[] substrings = lines[0].Split(',');

				int gridWidth = Int32.Parse(substrings[0]);
				gridHeight = Int32.Parse(substrings[1]);
				grid.Resize(gridWidth);
				foreach (List<Tile> list in grid)
					list.Resize(gridHeight);

				for (int i = 0; i < lines.Length - 1; i++) {
					substrings = lines[i + 3].Split(',');
					for (int j = 0; j < substrings.Length - 1; j++) {
						string[] pairstrings = substrings[j].Split(' ');
						grid[j][i].type = Int32.Parse(pairstrings[0]);
                        grid[j][i].subType = Int32.Parse(pairstrings[1]);
					}
				}
			}
		}
	}
}

public static class ListExtra {
	public static void Resize<T>(this List<T> list, int sz, T c) where T : new() {
		int cur = list.Count;
		if (sz < cur)
			list.RemoveRange(sz, cur - sz);
		else if (sz > cur) {
			if (sz > list.Capacity)
				list.Capacity = sz;
			for (int i = 0; i < sz - cur; i++) {
				list.Add(new T());
            }
		}
	}
	public static void Resize<T>(this List<T> list, int sz) where T : new() {
		Resize(list, sz, new T());
	}
}